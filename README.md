# Website

Website for the LBI project

This website was developed in _Django_ - a framework for web development, written in _Python_, which uses the model-template-view standard,

And implemented with _Heroku_ - a cloud platform as a service that supports several programming languages.

Link to access the site will be made available later.

This site allows the visitor to see and interact with the results obtained in the project,

Choosing which data set he wants to use, as well as the percentage of missing data and the method.
