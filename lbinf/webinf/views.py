from django.shortcuts import render

# Create your views here.

from webinf.models import Dataset, Paper, Method, Percentage, Original_Trees, Random_Trees, Intro_MissingData, IQTree

def index(request):
	"""View function for home page of site."""
	
	# Generate counts of some of the main objetcs
	num_datasets = Dataset.objects.all().count()
	
	# The 'all()' is implied by default
	num_papers = Paper.objects.count()
	
	context = {
		'num_datasets': num_datasets,
		'num_papers': num_papers,
	}
	
	# Render the HTML template index.html with the data in the context variable
	return render(request, 'index.html', context=context)
	
	
