from django.db import models
from django.conf import settings
from separatedvaluesfield.models import SeparatedValuesField

# Create your models here.

# DATASET
class Dataset(models.Model):
#	"""Model representing a dataset"""
	name = models.CharField(max_length=200)
	
	# Foreign Key used because dataset can only have one paper, but papers can have multiple 		  datasets
	# Paper as a string rather than object because it hasn't been declared yet in the file
	paper = models.ForeignKey('Paper', on_delete=models.CASCADE, null=False)
	
	outgroup = models.CharField('Outgroup',max_length=100, unique=True)
	
	sequences = models.CharField('Sequences',max_length=1000, unique=True)
	#Field.default(max_length=100)
	#acession_numbers = SeparatedValuesField(token=',')
	
	def __str_(self):
#	"""String for representing the Model object."""
		return self.name
	
	def get_absolute_url(self): 
#	"""Returns the url to access a detail record for this dataset.""" 
		return reverse('dataset-detail', args=[str(self.id)]) 
	
# PAPER
class Paper(models.Model):
#	"""Model representing a paper"""
	title = models.CharField(max_length=200)
	
	link = models.URLField('DOI', max_length=128, db_index=True, unique=True)
	
	authors_paper = models.CharField('Authors',max_length=500, unique=True)
	#Field.default(max_lenght=200)
	#authors = SeparatedValuesField(token=',')

	def __str__(self):
#	"""String for representing the Model object."""
		return self.title
	
# METHOD
class Method(models.Model):
	MAXIMUM_LIKELYHOOD = 'ML'
	BAYESIAN_INFERENCE = 'BI'
	METHOD = [(MAXIMUM_LIKELYHOOD, 'Maximum Likelyhood'),(BAYESIAN_INFERENCE, 'Bayesian Inference')]
	
	method = models.CharField(max_length=2, choices=METHOD)
	
	def is_upperclass(self):
		return self.method in {self.MAXIMUM_LIKELYHOOD, self.BAYESIAN_INFERENCE}

# choices.py
# from django_choice import DjangoChoice, DjangoChoices

# class StudentYearChoice(DjangoChoices):
#    FRESHMAN = DjangoChoice('FR')
#    SOPHOMORE = DjangoChoice('SO')
#    JUNIOR = DjangoChoice('JR')
#    SENIOR = DjangoChoice('SR')	

# from .choices import StudentYearChoice

# class Student(models.Model):
#    year_in_school = models.CharField(
#        max_length=2,
#        choices=StudentYearChoice.CHOICES,
#        default=StudentYearChoice.FRESHMAN,
#    )

#    def is_upperclass(self):
#        return self.year_in_school in (
#            StudentYearChoice.JUNIOR,
#            StudentYearChoice.SENIOR,
#        )
		
	def __str__(self):
#	"""String for representing the Model object."""
		return self.method
	
# PERCENTAGE
class Percentage(models.Model):
	
	class Percentage_choice(models.IntegerChoices):
		PERCENTAGE_10 = 10
		PERCENTAGE_15 = 15
		PERCENTAGE_20 = 20
	
#	PERCENTAGE =[(PERCENTAGE_10, 10),(PERCENTAGE_15, 15),(PERCENTAGE_20, 20)]
	
	percentage = models.IntegerField(choices=Percentage_choice.choices)
	
#	def __str__(self):
#	"""String for representing the Model object."""
#		return self.percentage
	
# ORIGINAL TREES

import uuid

class Original_Trees(models.Model):
#	"""Model representing the original trees"""
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular dataset across whole project') 
	dataset_OT = models.ForeignKey('Dataset', on_delete=models.PROTECT, null=True, related_name='dataset_OT')
	originalMethod = models.ForeignKey('Method', on_delete=models.PROTECT, null=True, related_name='originalMethod')
	image_OT = models.ImageField(help_text='Image of the phylogenetic tree')
	
	def __str__(self):
#	"""String for representing the Model object."""
		return f'{self.id} ({self.dataset.name})'
	
# MISSING DATA
class Intro_MissingData(models.Model):
#	"""Model representing the datasets with missing data"""
#	dataset_MD = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	dataset_MD = models.ForeignKey('Dataset', on_delete=models.PROTECT, null=True, related_name='dataset_MD')
	missingdata = models.ForeignKey('Percentage', on_delete=models.PROTECT, null=True, related_name='missingdata')
	
	def __str__(self):
#	"""String for representing the Model object."""
		return f'{self.id} ({self.dataset_MD.name})'

# RANDOM TREES	
class Random_Trees(models.Model):
#	"""Model representing the trees with missing data"""
	dataset_RT = models.ForeignKey('Intro_MissingData', on_delete=models.PROTECT, null=True, related_name='dataset_RT')
	randoMethod = models.ForeignKey('Method', on_delete=models.PROTECT, null=True, related_name='randoMethod')
	image_RT = models.ImageField(help_text='Image of the phylogenetic tree of a dataset with missing data')
	
	def __str__(self):
#	"""String for representing the Model object."""
		return f'{self.id} ({self.dataset_RT.name})'
	
# IQTree
class IQTree(models.Model):
	table = models.FileField(help_text='Pairwise comparison table')
	
	def __str__(self):
#	"""String for representing the Model object."""
		return self.table
	
