from django.apps import AppConfig


class WebinfConfig(AppConfig):
    name = 'webinf'
