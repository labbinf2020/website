from django.contrib import admin

# Register your models here.

from webinf.models import Dataset, Intro_MissingData, IQTree, Method, Paper, Percentage, Random_Trees, Original_Trees

# admin.site.register(Dataset)
admin.site.register(Intro_MissingData)
admin.site.register(IQTree)
admin.site.register(Method)
# admin.site.register(Paper)
admin.site.register(Percentage)
admin.site.register(Random_Trees)
admin.site.register(Original_Trees)

# Define the admin class
class PaperAdmin(admin.ModelAdmin):
	list_display = ('title', 'link')

# Register the admin class with the associated model
admin.site.register(Paper, PaperAdmin)

# Register the Admin classes for Dataset using the decorator
@admin.register(Dataset)
class DatasetAdmin(admin.ModelAdmin):
	list_display = ('name', 'paper', 'outgroup')
