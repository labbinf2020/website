from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
# import
# import
# import

#def index(request):
#    return HttpResponse("Hello world, this is our website for the LBINF project.")

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import Choice, Question


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    ... # same as above, no changes needed.
   
# HOME_PAGE
# def responder a let's start(request):
#	variavel=
#	html_datasetPage="<html><body>Select Dataset</body><html>"
#	return HttpResponse(html_datasetPage)

# def responder team(request):
#	...
#	return HttpResponse(html_teamPage)

# DATASET_PAGE
# def responder a um select dataset(request):
#	variavel=
#	html_percentagePage="<html><body>Select Percentage of Missing Data</body><html>"
#	return HttpResponse(html_percentagePage)

# PERCENTAGE_PAGE
# def responder a um select percentage(request):
#	variavel=
#	html_methodPage="<html><body>Select Method</body><html>"
#	return HttpResponse(html_methodPage)

# METHOD_PAGE
# def responder a um select method(request):
# retorna duas imagens
# retorna um valor
# retorna um parágrafo
# ... para cada situação relacionada ás anteriores
#	variavel=
#	html_resultsPage="<html><body>Results</body><html>"
# image
# image
# string
# string
#	return HttpResponse(htmlresultsPage)

# RESULT_PAGE
# def responder return to initial page(request):
#	return HttpResponse(html_homePage)
#
# def responder team(request):
# escrever a classe, o scope, os members/team e o DOI of paper
# 	html_resultsPage="<html><body>Results</body><html>"
# 	return HttpResponse(html_teamPage)
